============
Installation
============

At the command line::

    $ pip install namos

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv namos
    $ pip install namos
